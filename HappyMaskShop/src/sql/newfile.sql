
/* Drop Tables */

DROP TABLE ItemVenda CASCADE CONSTRAINTS;
DROP TABLE Venda CASCADE CONSTRAINTS;
DROP TABLE Cliente CASCADE CONSTRAINTS;
DROP TABLE Mascara CASCADE CONSTRAINTS;




/* Create Tables */

CREATE TABLE Cliente
(
	idCliente number NOT NULL,
	Nome varchar2(30),
	email varchar2(100) UNIQUE,
	senha varchar2(30) NOT NULL,
	PRIMARY KEY (idCliente)
);


CREATE TABLE ItemVenda
(
	idMascara number NOT NULL ,
	idVenda number NOT NULL,
	valor number NOT NULL
);


CREATE TABLE Mascara
(
	idMascara number NOT NULL ,
	nome varchar2(50) NOT NULL,
	preco float NOT NULL,
	propriedade varchar2(5000),
	material varchar2(50) NOT NULL,
        qtdEstoque number,
	PRIMARY KEY (idMascara)
);


CREATE TABLE Venda
(
	idVenda number NOT NULL,
	data date NOT NULL,
	quantidade number,
	formaDePagamento varchar2(500),
	idCliente number NOT NULL,
	PRIMARY KEY (idVenda)
);



/* Create Foreign Keys */

ALTER TABLE Venda
	ADD FOREIGN KEY (idCliente)
	REFERENCES Cliente (idCliente)
;


ALTER TABLE ItemVenda
	ADD FOREIGN KEY (idMascara)
	REFERENCES Mascara (idMascara)
;


ALTER TABLE ItemVenda
	ADD FOREIGN KEY (idVenda)
	REFERENCES Venda (idVenda)
;



