/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Helper {

    public static ResultSet load(String sql) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        ResultSet rs;

        try {
            ps = dbConnection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static PreparedStatement getPs(String sql){
        
        try {
            Conexao c = new Conexao();
            Connection dbConnection = c.getConexao();
            return dbConnection.prepareStatement(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return null;
    }

    public static void insert(PreparedStatement ps) {

        ResultSet rs;
        try {

            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }

    }
    public static ResultSet select(String sql) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {
            ps = dbConnection.prepareStatement(sql);

            // observe que uso o método execute update
            return ps.executeQuery();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static void update(String sql) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {
            ps = dbConnection.prepareStatement(sql);

            // observe que uso o método execute update
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
