/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happymaskshop;

import Classes.ItemvendaBd;
import Classes.MascaraBd;
import Classes.ClienteBd;
import Classes.VendaBd;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class VendaController implements Initializable {

    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField clienteTextField;
    @FXML
    private TextField choiceProduto;
    @FXML
    private TextField quantTextField;
    @FXML
    private Button addButton;
    @FXML
    private TableView<ItemvendaBd> maskTableView;
    @FXML
    private TableColumn<MascaraBd, String> produtoCol;
    @FXML
    private TableColumn<MascaraBd, Integer> quantCol;
    @FXML
    private TableColumn<MascaraBd, Float> precoCol;
    @FXML
    private TableColumn<MascaraBd, Double> totalCol;
    @FXML
    private Button comprarButton;
    
    private ObservableList<ItemvendaBd>itensVenda;
    @FXML
    private RadioButton cartaoRadio;
    @FXML
    private RadioButton chequeRadio;
    @FXML
    private RadioButton dinRadio;
   // private Object grupo;
    @FXML
    private ToggleGroup grupo;
    private VendaBd venda = new VendaBd();
    private MascaraBd mascara = new MascaraBd();
    private ClienteBd cliente = new ClienteBd();
    
    int x;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        itensVenda = (ObservableList<ItemvendaBd>) maskTableView.getItems();
        
        produtoCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        quantCol.setCellValueFactory(new PropertyValueFactory<>("qtd"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("preco"));
        totalCol.setCellValueFactory(new PropertyValueFactory<>("total"));
        carregarCarrinho();
    }

     private void salvarCarrinho() {
        try {
            FileOutputStream fout = new FileOutputStream("venda.fabricio");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            System.out.println(itensVenda);
            oos.writeObject(new ArrayList<ItemvendaBd>(itensVenda));
            oos.close();
            fout.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 
     private void carregarCarrinho() {
        try {
            FileInputStream fout = new FileInputStream("venda.fabricio");
            ObjectInputStream oos = new ObjectInputStream(fout);
            ArrayList<ItemvendaBd> lista = (ArrayList<ItemvendaBd>) oos.readObject();
            for(ItemvendaBd iv : lista){
                itensVenda.add(iv);
            }
            oos.close();
            fout.close();
        } catch (EOFException | NotSerializableException | FileNotFoundException ex) {
            salvarCarrinho();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    
    @FXML
    private void trocaTela() {
        Main.trocaTela("EntradaFXML");
    }
    
    @FXML
    public void addMascara(){
    ItemvendaBd iv = new ItemvendaBd();
    ArrayList<MascaraBd> mascs = MascaraBd.getAll();
    MascaraBd mx = null;
    for(MascaraBd masc : mascs){
        
        if(masc.getNome().equals(choiceProduto.getText())) mx = masc;
        
    }
    if(mx == null) return;
//    iv.setIdmascara(masc);
    iv.setMascara(mx);
    iv.setQtd(Integer.parseInt(quantTextField.getText()));
//    iv.setData(Date.valueOf(datePicker.getValue()));
//    //iv.setDate(LocalDate date = datePicker.getValue());
    if(cartaoRadio.isSelected()){
       x=1; 
        
    }else if(chequeRadio.isSelected()){
        x=2;
    }else if(dinRadio.isSelected()){
        x=3;
//    
    }else{System.out.println("Selecione alguma forma de pagamento!");}
//    
    itensVenda.add(iv);
    //iv.getValor()
    //String ; 
    //logger.escreve("\n O cliente " + cliente.getNome() + " adicionou ao seu carrinho de compras o album chamado:"+ " " + selectedItem.getNome() + ", que foi feito pelo artista " + selectedItem.getMusico());
    
//    datePicker.getEditor().clear();
//    clienteTextField.clear();;
    //grupo.selectToggle(null);
    } 

    @FXML
    private void comprar(ActionEvent event) {
            //venda.setQuantidade(Integer.valueOf(quantTextField.getText()));
            venda.setData(Date.valueOf(datePicker.getValue()));
            venda.insert();
            if(x==1){String log = " Cliente " + cliente.getNome() + " comprou: ";
        
            log += "  " + mascara.getNome() + " através de cartão na data " + venda.getData();
            Logger.createLog("logs.txt", log);}else if(x==2){
                String log = " Cliente " + cliente.getNome() + " comprou: ";
        
            log += "  " + mascara.getNome() + " através de cheque na data " + venda.getData();
            
            }else if(x==3){
            String log = " Cliente " + cliente.getNome() + " comprou: ";
        
            log += "  " + mascara.getNome() + " através de dinheiro na data " + venda.getData();
            }

            clienteTextField.clear();
            datePicker.getEditor().clear();
            itensVenda.clear();
            File file = new File ("venda.fabricio");
            file.delete();
    }
    
}
  
/*LocalDate value = datePicker.getValue();
 https://gitlab.com/guerreirodosul01/terceirotrimestre*/  

