/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happymaskshop;

import Classes.MascaraBd;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class CadastroProdutoController implements Initializable {
    
    @FXML
    private TextField materialText;
    @FXML
    private TextField precoText;
    @FXML
    private Button cadButton;
    
    @FXML
    private TextField nomeText;
    @FXML
    private TextArea propriedadeTextArea;
    @FXML
    private TextField qtdText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // TODO
    }    
    
    @FXML
    private void cadastrar(ActionEvent event) {
        
        MascaraBd mascara = new MascaraBd();
        mascara.setNome(nomeText.getText());
        mascara.setMaterial(materialText.getText());
        mascara.setPreco(Float.valueOf(precoText.getText()));
        mascara.setPropriedade(propriedadeTextArea.getText());
        mascara.setQtdEstoque(Integer.parseInt(qtdText.getText()));
        mascara.insert();
        
    }
    
    @FXML
    private void trocaTela() {
        Main.trocaTela("EntradaFXML");
    }
    
}
