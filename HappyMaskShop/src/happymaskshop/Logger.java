/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happymaskshop;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author fabricio
 */
public class Logger {

    public static void createLog(String fileName, String content) {
        File file = new File(fileName);
        try {

            FileWriter fr = new FileWriter(file, true);
            fr.write("[" + new Date() + "] - " +content + "\r\n");
            fr.close();
            
        } catch (Exception e) {

        }
    }
}

//logger.escreve("\n O cliente " + client.getNome() + " adicionou ao seu carrinho de compras o album chamado:"+ " " + selectedItem.getNome() + ", que foi feito pelo artista " + selectedItem.getMusico());