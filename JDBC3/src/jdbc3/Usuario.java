package jdbc3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;


public class Usuario {
    private String nome;
    private int senha;
    private String nomeInteresse;
    
    
    public int insert() {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_USUARIO_EXO"
                + "(id_usuario,nome, senha) VALUES"
                + "(seq_venda.nextval,?,?)";

        try {

            String generatedColumns[] = {"codigoVenda"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setInt(1, nome.);
            ps.setInt(2, venda.getCliente().getCodigoPessoa());
            ps.setDate(3, venda.gerarVenda());
            ps.setDouble(4, venda.getValorTotal()); //somar valores itens

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Venda table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            venda.setCodigoVenda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoVenda;
    }
    
    
    
    
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }

    public String getNomeInteresse() {
        return nomeInteresse;
    }

    public void setNomeInteresse(String nomeInteresse) {
        this.nomeInteresse = nomeInteresse;
    }
    
}
